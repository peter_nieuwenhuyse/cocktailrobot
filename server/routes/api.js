const express = require('express');
const router = express.Router();
const Cocktail = require('../models/cocktails');
const bodyParser = require('body-parser');
const Position = require('../models/positions');
const Order = require('../models/order');
const Ingredient = require('../models/ingredients');

var Promise = require('bluebird');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});

router.get('/cocktails', (req, res) => {

  Cocktail.find({}, function (err, cocktails) {
    if (err) throw err;
    res.json(cocktails);
  })
});

router.get('/positions', (req, res) => {
  Position.find({}, function (err, positions) {
    console.log(positions);
    if (err) throw err;
    res.json(positions)
  })
});

router.post('/positions/update', (req, res)=> {
  Ingredient.update(req.body)
    .then(item => {
      res.status(200)
    })
    .catch(err => {
      res.status(400)
    });
});

router.get('/ingredients', (req, res) => {
  Ingredient.find({}, function(err, ingredients){
    if(err) throw err;
    res.json(ingredients)
  } )
});

router.post('/cocktails', (req, res) => {
  const newCocktail = new Cocktail(req.body);
  newCocktail.save()
    .then(item => {
      res.status(200)
    })
    .catch(err => {
      res.status(400)
    });
});
router.post('/cocktails/delete', (req, res) => {
  const target = req.body.name;
  return Cocktail.findOneAndRemove({name: target}, function () {
  })
    .then(item => {
      res.status(200)
    })
    .catch(err => {
      res.status(400)
    });//does not work without providing a dummy function

});

router.post('/cocktails/update', (req, res) => {
  const target = req.body.name;
  Cocktail.findOneAndUpdate({name: target}, req.body, function () {
  })
    .then(item => {
      res.status(200)
    })
    .catch(err => {
      res.status(400)
    });//does not work without providing a dummy function
});

router.post('/orders', (request, response) => {
  const newOrder = new Order(request.body);
  console.log(request.body)
  console.log(newOrder);
  newOrder.creationDate = new Date();
  newOrder.completed = false;
  newOrder.save()
    .then(item => {
      response.status(201).send(item);
    })
    .catch(error => {
      response.status(400).send(error);
    })
});

module.exports = router;
