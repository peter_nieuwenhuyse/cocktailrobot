const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//creating the cocktail schema

const positionSchema = new Schema({
  index: Number,
  ingredient: String,
  capacityLeft: Number
});

const Position = mongoose.model('positions', positionSchema);

module.exports = Position;
