const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//creating the cocktail schema

const ingredientSchema = new Schema({
  name: String,
  totalCapacity: Number,
  colour: String
});

const Ingredient = mongoose.model('ingredients', ingredientSchema);

module.exports = Ingredient;
