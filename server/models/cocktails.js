const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//creating the cocktail schema

const cocktailschema = new Schema({
  name:String,
  parts: [
    {
      ingredient: String,
      amount: Number
    }
  ]
});

const Cocktail = mongoose.model('Cocktails', cocktailschema);

module.exports = Cocktail;
