const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//creating the cocktail schema

const orderSchema = new Schema({
  cocktailId: String,
  userName: String,
  userEmail: String,
  creationDate: Date,
  completed: Boolean
});

const Order = mongoose.model('orders', orderSchema);

module.exports = Order;
