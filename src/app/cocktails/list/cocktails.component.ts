import { Component, OnInit } from '@angular/core';
import {CocktailsService} from '../service/cocktails.service';
import {Cocktail} from '../model/cocktail';

@Component({
  selector: 'app-cocktails',
  templateUrl: './cocktails.component.html',
  styleUrls: ['./cocktails.component.css']
})
export class CocktailsComponent implements OnInit {
  cocktails: Cocktail[] = [];
  errorMessage: string;

  constructor(
    private cocktailservice: CocktailsService
  ) { }

  ngOnInit() {
    this.getCocktails();
  }

  private getCocktails(): void {
    this.cocktailservice.getAllCocktails()
      .subscribe(cocktailList => this.cocktails = cocktailList, error => this.errorMessage = error);
  }
}
