import { Component, OnInit } from '@angular/core';
import {CocktailsService} from '../service/cocktails.service';
import {Cocktail} from '../model/cocktail';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import { Location } from '@angular/common';

@Component({
  selector: 'app-cocktails-details',
  templateUrl: './cocktails-details.component.html',
  styleUrls: ['./cocktails-details.component.css']
})
export class CocktailsDetailsComponent implements OnInit {
  cocktail: Cocktail;
  constructor(
    private router: Router,
    private cocktailService: CocktailsService,
    private route: ActivatedRoute,
    private location: Location
  ) {}
  ngOnInit(): void {
    this.route.params
      .switchMap(
        (params: ParamMap) => {
          if (params['id']) {
            return this.cocktailService.getCocktail(params['id']);
          }
          return Observable.throw(new Error('id was expected but not specified'));
        }
      )
      .subscribe((cocktail: any) => {
        if (cocktail ) {
          this.cocktail = cocktail;
        }else {
          this.router.navigate(['*']);
        }
      });
  }
  goBack(): void {
    this.location.back();
  }
}
