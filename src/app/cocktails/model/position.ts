export interface Position {
  id: number;
  ingredient: string;
  capacityLeft: number;
}
