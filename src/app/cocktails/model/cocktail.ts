export interface Cocktail {
    id: string;
    name: string;
    parts: Parts[];
}

export interface Parts {
  ingredient: string;
  amount: number;
}
