export interface Ingredient {
  id: string;
  name: string;
  totalCapacity: number;
  colour: string;
}
