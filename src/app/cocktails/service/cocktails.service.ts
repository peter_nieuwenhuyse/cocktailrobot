import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cocktail } from '../model/cocktail';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

/*interface CocktailListRespons {
  cocktailList: Cocktail[];
}*/

interface CocktailResponse {
  cocktail: Cocktail;
}

@Injectable()
export class CocktailsService {
  private allcocktailsUrl = 'api/cocktails';
  private api = 'api/cocktails';

  constructor(private _http: HttpClient) { }

  getAllCocktails(): Observable<Cocktail[]> {
    return this._http.get<Cocktail[]>(this.allcocktailsUrl);
  }

  getCocktail(id: string): Promise<Cocktail> {
    const url = `${this.api}/${id}`;
    return this._http
      .get<CocktailResponse>(url)
      .toPromise()
      .then(response => response.cocktail);
  }

  addCocktail(cocktail) {
    this._http.post(this.api, cocktail).subscribe();
  }

  removeCocktail(target) {
    const body = {name: target};
    this._http.post(this.api + '/delete', body ).subscribe();
  }

  updateCocktail(cocktail) {
    this._http.post(this.api + '/update', cocktail).subscribe();
  }
}
