import { TestBed, inject } from '@angular/core/testing';

import { CocktailsService } from './cocktails.service';

describe('CocktailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CocktailsService]
    });
  });

  it('should be created', inject([CocktailsService], (service: CocktailsService) => {
    expect(service).toBeTruthy();
  }));
});
