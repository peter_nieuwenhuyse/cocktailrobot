import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CocktailsComponent } from './cocktails/list/cocktails.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CocktailsService } from './cocktails/service/cocktails.service';
import { HttpClientModule } from '@angular/common/http';
import { AddCocktailComponent } from './admin/add-cocktail/add-cocktail.component';
import { EditCoctailsComponent } from './admin/edit-cocktails/edit-cocktails.component';
import { EditSingleCocktailComponent } from './admin/edit-single-cocktail/edit-single-cocktail.component';
import { OrderformComponent } from './orders/orderform/orderform.component';
import { OrdersService } from './orders/service/orders.service';
import { ManagePossitionsComponent } from './admin/manage-positions/manage-positions.component';
import { PositionsService } from './admin/services/positions.service';
import { IngredientsService } from './admin/services/ingredients.service';
import {CocktailsDetailsComponent} from './cocktails/detail/cocktails-details.component';

@NgModule({
  declarations: [
    AppComponent,
    CocktailsComponent,
    PageNotFoundComponent,
    AddCocktailComponent,
    EditCoctailsComponent,
    EditSingleCocktailComponent,
    OrderformComponent,
    ManagePossitionsComponent,
    CocktailsDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
        {path: 'admin/positions', component: ManagePossitionsComponent},
        {path: 'order/:id', component: OrderformComponent},
        {path: 'addCocktail', component: AddCocktailComponent},
        {path: 'cocktail/:id', component: CocktailsDetailsComponent},
        {path: '', component: CocktailsComponent},
        {path: '**', component: PageNotFoundComponent}
    ])
  ],
  providers: [
    CocktailsService,
    OrdersService,
    PositionsService,
    IngredientsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
