import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import {CocktailsService} from '../../cocktails/service/cocktails.service';

@Component({
  selector: 'app-add-cocktail',
  templateUrl: './add-cocktail.component.html',
  styleUrls: ['./add-cocktail.component.css']
})
export class AddCocktailComponent implements OnInit {

  public addCocktailForm: FormGroup;

  constructor(private _fb: FormBuilder, private cocktailservice: CocktailsService) { }

  ngOnInit() {
    this.addCocktailForm = this._fb.group({
      name: '',
      parts: this._fb.array([this.initIngredients()]),
    });
  }

  initIngredients() {
    return this._fb.group({
      name: '',
      amount : 0
    });
  }

  addNewRow() {
    const control = <FormArray>this.addCocktailForm.controls['ingredients'];
    control.push(this.initIngredients());
  }

  deleteRow(index: number) {
    const control = <FormArray>this.addCocktailForm.controls['ingredients'];
    control.removeAt(index);
  }

  onSubmit(cocktail) {
    this.cocktailservice.addCocktail(cocktail.value);
  }
}
