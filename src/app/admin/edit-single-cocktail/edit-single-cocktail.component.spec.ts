import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSingleCocktailComponent } from './edit-single-cocktail.component';

describe('EditSingleCocktailComponent', () => {
  let component: EditSingleCocktailComponent;
  let fixture: ComponentFixture<EditSingleCocktailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSingleCocktailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSingleCocktailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
