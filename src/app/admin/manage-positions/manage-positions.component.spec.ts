import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePossitionsComponent } from './manage-positions.component';

describe('ManagePossitionsComponent', () => {
  let component: ManagePossitionsComponent;
  let fixture: ComponentFixture<ManagePossitionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagePossitionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePossitionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
