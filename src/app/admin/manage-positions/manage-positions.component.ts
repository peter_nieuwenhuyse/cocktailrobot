import { Component, OnInit } from '@angular/core';
import { Position } from '../../cocktails/model/position';
import { Ingredient } from '../../cocktails/model/ingredient';
import {IngredientsService} from '../services/ingredients.service';
import {PositionsService} from '../services/positions.service';


@Component({
  selector: 'app-manage-possitions',
  templateUrl: './manage-positions.component.html',
  styleUrls: ['./manage-positions.component.css']
})
export class ManagePossitionsComponent implements OnInit {
  positions: Position[] = [];
  ingredients: Ingredient[] = [];
  errorMessage: string;

  constructor(private ingredientservice: IngredientsService, private positionservice: PositionsService) { }

  ngOnInit() {
    this.getIngredients();
    this.getPositions();
    console.log(this.positions);
  }

  updatePositions(data) {
    console.log(data.value);
  }

  private getIngredients(): void {
    this.ingredientservice.getIngredients()
      .subscribe(ingredientList => this.ingredients = ingredientList, error => this.errorMessage = error);
  }

  private getPositions(): void {
    this.positionservice.getPositions()
      .subscribe(positionList => this.positions = positionList, error => this.errorMessage = error);
  }

}
