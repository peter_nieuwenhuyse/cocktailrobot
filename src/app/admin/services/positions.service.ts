import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Position} from '../../cocktails/model/position';

@Injectable()
export class PositionsService {
  private allpositionssUrl = 'api/positions';
  constructor( private _http: HttpClient) { }

  getPositions(): Observable<Position[]> {
    return this._http.get<Position[]>(this.allpositionssUrl);
  }

  updatePositions(positions) {
    this._http.post(this.allpositionssUrl, positions).subscribe();
  }
}
