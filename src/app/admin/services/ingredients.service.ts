import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Ingredient } from '../../cocktails/model/ingredient';


@Injectable()
export class IngredientsService {
  private allIngredientsUrl = 'api/ingredients';
  constructor( private _http: HttpClient) { }

  getIngredients(): Observable<Ingredient[]> {
    return this._http.get<Ingredient[]>(this.allIngredientsUrl);
  }
}
