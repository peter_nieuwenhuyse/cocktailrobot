import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCoctailsComponent } from './edit-cocktails.component';

describe('EditCoctailsComponent', () => {
  let component: EditCoctailsComponent;
  let fixture: ComponentFixture<EditCoctailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCoctailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCoctailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
