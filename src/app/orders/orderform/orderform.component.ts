import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup} from '@angular/forms';
import { OrdersService} from '../service/orders.service';
import { ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-orderform',
  templateUrl: './orderform.component.html',
  styleUrls: ['./orderform.component.css']
})
export class OrderformComponent implements OnInit {

  public orderCocktail: FormGroup;
  public cocktailId: string;
  constructor(private _fb: FormBuilder,
              private orderService: OrdersService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.cocktailId = params['id'];
    });
    this.orderCocktail = this._fb.group({
      userName: '',
      userEmail: ''
    });

  }
  onSubmit(data) {
    const order = data.value;
    order.cocktailId = this.cocktailId;
    this.orderService.newOrder(order);
    this.router.navigateByUrl('/');
  }
}
