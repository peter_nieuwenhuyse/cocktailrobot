import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class OrdersService {
  private ordersUrl = 'api/orders';
  constructor(private _http: HttpClient) { }

  newOrder(order) {
    console.log(order);
    this._http.post(this.ordersUrl, order).subscribe();
  }
}
