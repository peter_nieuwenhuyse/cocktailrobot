# Coctailbot

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.9.

## Getting started

* clone repo

* in terminal navigate to folder coctailbot

* run npm install

* new terminal : start mongo command : `mongod`

* another terminal : start mongo shell : command: `mongo

* in mongo shell: 

* create database : `use Cocktails`

* create collection cocktails: `db.createCollection('cocktails')`

* create collection ingredients: `db.createCollection('ingredients')`

* create collection positions: `db.createCollection('positions')`

* insert data in collections: `db.cocktails.insert([
                                                     {
                                                       "name": "Cuba Libre",
                                                       "parts": [
                                                         {
                                                           "ingredient": "Brown Rum",
                                                           "amount": 1
                                                         },
                                                         {
                                                           "ingredient": "Coke",
                                                           "amount": 3
                                                         },
                                                         {
                                                           "ingredient": "Lime juice",
                                                           "amount": 1
                                                         }
                                                       ]
                                                     },
                                                     {
                                                       "name": "Daiquiri",
                                                       "parts": [
                                                         {
                                                           "ingredient": "White Rum",
                                                           "amount": 2
                                                         },
                                                         {
                                                           "ingredient": "Cane sugar syrup",
                                                           "amount": 1
                                                         },
                                                         {
                                                           "ingredient": "Lime juice",
                                                           "amount": 1
                                                         }
                                                       ]
                                                     }
                                                   ])`

* insert ingredients: `db.ingredients.insert([
                                                                           {
                                                                             "name": "Brown Rum",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "White Rum",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Vodka",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Peach schnapps",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Cointreau",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Dry vermouth",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Coffee liquor",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Gin",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Tonic",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Carbonated water",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Coke",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Grenadine",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Full-cream milk",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Ginger ale",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Sugar cane syrup",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Coco Cream",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Lime juice",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Multi fruit juice",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Passion fruit juice",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Pineapple juice",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Cranberry juice",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           },
                                                                           {
                                                                             "name": "Raspberry juice",
                                                                             "totalCapacity": 750,
                                                                             "colour": "#ffffff"
                                                                           }
                                                                         ])`
                                                                         
 * insert positions: `db.positions.insert([
                                           {
                                             "index": 1,
                                             "ingredient": "Brown Rum",
                                             "capacityLeft": 750
                                           },
                                           {
                                             "index": 2,
                                             "ingredient": "White Rum",
                                             "capacityLeft": 750
                                           },
                                           {
                                             "index": 3,
                                             "ingredient": "Vodka",
                                             "capacityLeft": 750
                                           },
                                           {
                                             "index": 4,
                                             "ingredient": "Gin",
                                             "capacityLeft": 750
                                           },
                                           {
                                             "index": 5,
                                             "ingredient": "Tonic",
                                             "capacityLeft": 750
                                           },
                                           {
                                             "index": 6,
                                             "ingredient": "Coke",
                                             "capacityLeft": 750
                                           },
                                           {
                                             "index": 7,
                                             "ingredient": "Grenadine",
                                             "capacityLeft": 750
                                           },
                                           {
                                             "index": 8,
                                             "ingredient": "Sugar cane syrup",
                                             "capacityLeft": 750
                                           },
                                           {
                                             "index": 9,
                                             "ingredient": "Lime juice",
                                             "capacityLeft": 750
                                           },
                                           {
                                             "index": 10,
                                             "ingredient": "Multi fruit juice",
                                             "capacityLeft": 750
                                           }
                                         ]
)`

* create dist folder: back in terminal in root folder project : `ng build`

* start backend server: `node server.js` ( or use nodemon server.js)

* surf to localhost:3000 the application should run now
     
